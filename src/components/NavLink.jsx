import { Link, useMatch } from 'react-router-dom';

export const NavLink = ({ children, to, ...props }) => {
  const matched = useMatch({
    path: to,
    end: to.length === 1,
  });

  return (
    <Link
      to={to}
      style={{
        color: matched ? '#fff' : '#7da3a1',
        backgroundColor: 'transparent',
        textDecoration: 'none',
        fontSize: '18px',
        margin: '0 10px',
      }}
      {...props}
    >
      {children}
    </Link>
  );
};

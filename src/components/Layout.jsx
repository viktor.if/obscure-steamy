import { Outlet } from 'react-router-dom';
import { NavLink } from './NavLink';

export const Layout = () => (
  <>
    <header>
      <NavLink to="/">Home</NavLink>
      <NavLink to="/posts">Blog</NavLink>
      <NavLink to="/about">About</NavLink>
    </header>

    <Outlet />
  </>
);

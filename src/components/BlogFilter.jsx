import { useState } from 'react';

export const BlogFilter = ({ setSearchParams, postQuery, latest }) => {
  const [search, setSearch] = useState(postQuery);
  const [checked, setChecked] = useState(latest);

  const handleSubmit = (event) => {
    event.preventDefault();
    const form = event.target;
    const query = form.search.value;
    const isLatest = form.latest.checked;

    const params = {};

    if (query.length) {
      params.post = query;
    }

    if (isLatest) {
      params.latest = true;
    }

    setSearchParams(params);
  };

  return (
    <form autoComplete="off" onSubmit={handleSubmit} className="mg-16">
      <input
        type="search"
        name="search"
        value={search}
        onChange={(event) => setSearch(event.target.value)}
        className="text-input"
      />
      <label style={{ padding: '0 1rem' }}>
        {' '}
        New only
        <input
          type="checkbox"
          name="latest"
          checked={checked}
          onChange={(event) => setChecked(event.target.checked)}
          className="check-box"
        />
      </label>
      <input type="submit" value="Search" className="button" />
    </form>
  );
};

import { Link } from 'react-router-dom';

export const NotFoundPage = () => (
  <div className="mg-16">
    <h2>
      This page doesn't exist. Go <Link to="/">home</Link>
    </h2>
  </div>
);

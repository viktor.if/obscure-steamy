import { useSearchParams, Link } from 'react-router-dom';
import { BlogFilter } from '../components/BlogFilter';
import { useFetch } from '../hooks';

export const BlogPage = () => {
  const [searchParams, setSearchParams] = useSearchParams();

  const { data } = useFetch('https://jsonplaceholder.typicode.com/posts');

  const postQuery = searchParams.get('post') || '';
  const latest = searchParams.has('latest');

  const startsFrom = latest ? 80 : 1;

  if (!data?.length) {
    return <h1>Loading...</h1>;
  }

  return (
    <div>
      <h1>Our news</h1>

      <BlogFilter
        setSearchParams={setSearchParams}
        postQuery={postQuery}
        latest={latest}
      />

      <div className="mg-16">
        {data
          .filter(
            (item) => item.title.includes(postQuery) && item.id >= startsFrom
          )
          .map((item) => (
            <li key={item.id}>
              <Link to={`/posts/${item.id}`}>{item.title}</Link>
            </li>
          ))}
      </div>
    </div>
  );
};

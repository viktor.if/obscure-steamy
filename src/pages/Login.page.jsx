import { useLocation, useNavigate } from 'react-router-dom';
import { useAuth } from '../hooks';

export const LoginPage = () => {
  const navigate = useNavigate();
  const location = useLocation();

  const { login } = useAuth();

  const fromPage = location.state?.from?.pathname || '/';
  const handleSubmit = (event) => {
    event.preventDefault();

    const form = event.target;
    const user = form.username.value;

    login(user, () => navigate(fromPage, { replace: true }));
  };

  return (
    <div>
      <h1>Log In</h1>
      <form onSubmit={handleSubmit} className="mh-16">
        <label>
          Name: <input name="username" className="text-input mh-16" />
        </label>
        <button type="submit" className="button">
          Login
        </button>
      </form>
    </div>
  );
};

import { Link, useNavigate, useParams } from 'react-router-dom';

import { useFetch } from '../hooks';

export const PostSinglePage = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const goBack = () => navigate(-1);
  const goHome = () => navigate('/', { replace: true });

  const { data } = useFetch(`https://jsonplaceholder.typicode.com/posts/${id}`);

  if (!data) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <button onClick={goBack} className="button mg-16">
        Go back
      </button>
      <button onClick={goHome} className="button mg-16">
        Go home
      </button>

      <h1>{data.title}</h1>
      <p className="mg-16">{data.body}</p>

      <div className="mg-16">
        <li>
          <Link to={`/posts/${id}/edit`}>Edit this post</Link>
        </li>
        <li>
          <Link to={`/posts/new`}>Create new one</Link>
        </li>
      </div>
    </div>
  );
};

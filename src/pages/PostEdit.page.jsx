import { useParams } from 'react-router-dom';

export const PostEditPage = () => {
  const { id } = useParams();

  return (
    <div>
      <h1>Edit post with ID: {id}</h1>
    </div>
  );
};

import { NavLink, Outlet } from 'react-router-dom';

export const AboutPage = () => {
  return (
    <div>
      <h1>About page</h1>

      <div className="mh-16">
        <ul>
          <li className="mh-16">
            <NavLink to="contacts">Contacts</NavLink>
          </li>
          <li className="mh-16">
            <NavLink to="team">Team</NavLink>
          </li>
        </ul>
      </div>

      <div className="mg-16">
        <Outlet />
      </div>
    </div>
  );
};

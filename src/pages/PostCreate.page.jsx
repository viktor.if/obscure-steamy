import { useNavigate } from 'react-router-dom';
import { useAuth } from '../hooks';

export const PostCreatePage = () => {
  const { logout } = useAuth();
  const navigate = useNavigate();

  return (
    <div>
      <h2>Create new post</h2>
      <button onClick={() => logout(() => navigate('/', { replace: true }))}>
        Log out
      </button>
    </div>
  );
};

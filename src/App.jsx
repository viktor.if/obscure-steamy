import { Navigate, Route, Routes } from 'react-router-dom';

import './App.css';
import { Layout } from './components';
import { RequireAuth, AuthProvider } from './hocs';

import {
  AboutPage,
  BlogPage,
  HomePage,
  NotFoundPage,
  PostCreatePage,
  PostEditPage,
  PostSinglePage,
  LoginPage,
} from './pages';

function App() {
  return (
    <AuthProvider>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<HomePage />} />
          <Route path="about/*" element={<AboutPage />}>
            <Route path="contacts" element={<p>Our contacts</p>} />
            <Route path="team" element={<p>Our team</p>} />
          </Route>
          <Route path="about-us" element={<Navigate to="/about" replace />} />
          <Route path="posts" element={<BlogPage />} />
          <Route path="posts/:id" element={<PostSinglePage />} />
          <Route path="posts/:id/edit" element={<PostEditPage />} />
          <Route
            path="posts/new"
            element={
              <RequireAuth>
                <PostCreatePage />
              </RequireAuth>
            }
          />
          <Route path="login" element={<LoginPage />} />
          <Route path="*" element={<NotFoundPage />} />
        </Route>
      </Routes>
    </AuthProvider>
  );
}

export default App;

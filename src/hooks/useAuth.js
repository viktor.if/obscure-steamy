import { useContext } from "react"
import { AuthContext } from "../hocs/AuthProvider.hoc"

export const useAuth = () => {
  return useContext(AuthContext)
}
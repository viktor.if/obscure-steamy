import { useEffect, useState } from "react";

export const useFetch = (url) => {
  const [data, setData] = useState(null)
  const [error, setError] = useState(null)

  useEffect(() => {
    fetch(url)
      .then((resp) => resp.json())
      .then((json) => setData(json))
      .catch((err) => setError(err));
  }, []);

  return { data, error }
}
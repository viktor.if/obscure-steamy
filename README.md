Obscure Steamy
==============

Demonstrates capabilities of __React Route__ package

## Run locally

1. Install dependencies

`yarn install`

2. Start app

`yarn start`
